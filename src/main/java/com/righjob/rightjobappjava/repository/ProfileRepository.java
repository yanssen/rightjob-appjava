package com.righjob.rightjobappjava.repository;

import com.righjob.rightjobappjava.model.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileRepository extends JpaRepository<Profile, Long> {
}
