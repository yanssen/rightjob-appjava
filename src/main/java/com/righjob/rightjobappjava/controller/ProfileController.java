package com.righjob.rightjobappjava.controller;

import com.righjob.rightjobappjava.model.Profile;
import com.righjob.rightjobappjava.service.ProfileService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/profile")
@AllArgsConstructor
public class ProfileController {
    private final ProfileService profileService;

    @PostMapping("/create")
    public  Profile create(@RequestBody Profile profile){
        return profileService.create(profile);
    }
    @GetMapping("/view")
    public List<Profile> view(){
        return profileService.view();
    }
    @PutMapping("/modify/{id}")
    public Profile modify(@PathVariable Long id,@RequestBody Profile profile){
        return profileService.modify(id,profile);
    }
    @DeleteMapping("/delete")
    public String delete(@PathVariable Long id){
        return profileService.delete(id);
    }

}
