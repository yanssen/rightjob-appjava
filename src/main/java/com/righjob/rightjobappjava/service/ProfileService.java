package com.righjob.rightjobappjava.service;

import com.righjob.rightjobappjava.model.Profile;

import java.util.List;

public interface ProfileService {
    Profile create(Profile profile);

    List<Profile> view();

    Profile modify(Long id, Profile profile);

    String delete(Long id);



}
