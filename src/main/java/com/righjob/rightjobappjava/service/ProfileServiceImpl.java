package com.righjob.rightjobappjava.service;

import com.righjob.rightjobappjava.model.Profile;
import com.righjob.rightjobappjava.repository.ProfileRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@AllArgsConstructor
public class ProfileServiceImpl implements ProfileService{

    private final ProfileRepository  profileRepository;

    @Override
    public Profile create(Profile profile) {
        return profileRepository.save(profile);
    }

    @Override
    public List<Profile> view() {
        return profileRepository.findAll();
    }

    @Override
    public Profile modify(Long id, Profile profile) {
        return profileRepository.findById(id)
                .map(p -> {
                    p.setNom(profile.getNom());
                    p.setNom(profile.getPrenom());
                    return profileRepository.save(profile);
                }).orElseThrow(() -> new RuntimeException(" Profile not found "));
    }

    @Override
    public String delete(Long id) {
        profileRepository.deleteById(id);
        return "profile deleted";
    }
}
